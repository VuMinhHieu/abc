/*
 * PID.h
 *
 *  Created on: Feb 23, 2021
 *      Author: PC
 */

#ifndef INC_PID_H_
#define INC_PID_H_

typedef struct {

	/* Controller gains */
	float Kp;
	float Ki;
	float Kd;

	/* Derivative low-pass filter time constant */
	float tau;

	/* Output limits */
	float limMin;
	float limMax;

	/* Integrator limits */
//	float limMinInt;
//	float limMaxInt;

	/* Sample time (in seconds) */
	float Ts;

	/* Controller "memory" */
	float prevIntegrator;

	float integrator;
	float differentiator;
	float prevError;		     	/* Required for integrator */
	float prevMeasurement;		/* Required for differentiator */

	/* Controller output */
	float out;

} PIDController;

void  PIDController_Init(PIDController *pid);
float PIDController_Update(PIDController *pid, float setpoint, float measurement);

#endif /* INC_PID_H_ */
