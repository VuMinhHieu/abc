#include "PID.h"

void PIDController_Init(PIDController *pid) 
	{
	/* Clear controller variables */
	pid->integrator = 0.0f;
	pid->differentiator  = 0.0f;
	
	pid->prevError  = 0.0f;
	pid->prevMeasurement = 0.0f;
	pid->prevIntegrator = 0.0f;

	pid->out = 0.0f;
	return; 
}

float PIDController_Update(PIDController *pid, float setpoint, float measurement) 
	{
		float error = setpoint - measurement;
    float proportional = pid->Kp * error;
		
    pid->integrator = pid->prevIntegrator + 0.5f * pid->Ki * pid->Ts * (error + pid->prevError);

//    if (pid->integrator > pid->limMaxInt) 
//			{
//				pid->integrator = pid->limMaxInt;
//			} 
//		else if (pid->integrator < pid->limMinInt) 
//			{
//				pid->integrator = pid->limMinInt;
//			}

	/*
	* Derivative (band-limited differentiator)
	*/
		
    pid->differentiator = -(2.0f * pid->Kd * (measurement - pid->prevMeasurement)	/* Note: derivative on measurement, therefore minus sign in front of equation! */
                          + (2.0f * pid->tau - pid->Ts) * pid->differentiator)
                          / (2.0f * pid->tau + pid->Ts);


	/*
	* Compute output and apply limits
	*/
    pid->out = proportional + pid->integrator + pid->differentiator;

    if (pid->out > pid->limMax) {

        pid->out = pid->limMax;

    } else if (pid->out < pid->limMin) {

        pid->out = pid->limMin;

    } else {
				pid->prevIntegrator = pid->integrator;
		}
	/* Store error and measurement for later use */
		pid->prevIntegrator  = pid->integrator;
    pid->prevError       = error;
    pid->prevMeasurement = measurement;

	/* Return controller output */
    return pid->out;

}